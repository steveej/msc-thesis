#![feature(naked_functions)]
#![deny(unconditional_recursion)]

// #[derive(Debug)]
// struct Stat {
//     sum: isize,
//     count: isize,
//     avg: isize,
//     min: isize,
//     max: isize,
// }
//
// macro_rules! sum {
//     () => (0);
//     ($e:expr) => ( $e );
//     ($head:expr, $($tail:expr),*) => { $head + sum!($($tail),*)};
// }
//
// macro_rules! count {
//     () => (0);
//     ($e:expr) => ( 1 );
//     ($head:expr, $($tail:expr),*) => { 1 + count!($($tail),*) };
// }
//
// #[inline(never)]
// fn push(a: isize, b: isize, c: isize) -> isize {
//     sum!(a, b, c)
// }
//
// macro_rules! avg{
//     ($($all:expr),+) => {
//         sum!($($all),+) / count!($($all),+)
//     };
// }
//
// macro_rules! max {
//     ($x:expr) => ( $x );
//     ($x:expr, $($xs:expr),+) => {
//         {
//             use std::cmp::max;
//             max($x, max!( $($xs),+ ))
//         }
//     };
// }
//
// macro_rules! min {
//     ($x:expr) => ( $x );
//     ($x:expr, $($xs:expr),+) => {
//         {
//             use std::cmp::min;
//             min($x, min!( $($xs),+ ))
//         }
//     };
// }
// #[inline(never)]
// fn stats(a: isize, b: isize, c: isize) -> Stat {
//     let sum = sum(a, b, c);
//     let count = count!(a, b, c);
//     let avg = avg!(a, b, c);
//     let min = min!(a, b, c);
//     let max = max!(a, b, c);
//     Stat {
//         sum,
//         count,
//         avg,
//         min,
//         max,
//     }
// }
//
// #[inline(never)]
// fn sum(a: isize, b: isize, c: isize) -> isize {
// sum!(a, b, c)
// }
#[inline(never)]
fn passthrough(a: isize) -> isize {
    let b = a;
    b
}

#[inline(never)]
fn r();
    println!("main exiting")
}